Ethereum
====
A rede Ethereum é uma rede P2P cujo o principal objetivo é a criação de aplicações distribuídas (dapps) vizando o processamento de dados ou realização de operações em troca de uma criptomoeda, estas são apresentadas neste trabalho como contratos, as dapps são disponibilizadas para todos os usuário da rede, desde que o usuário saiba o seu endereço de acesso, para uso dessa aplicações o usuário se conecta a uma rede P2P global que permite, através de transações monetárias, fazer uso dessas aplicações.

Este trabalho tem como objetivo demonstrar a publicação de um contrato simples e a metodologia mais comum de acesso a contratos. Prática extremamente interessante para usuários que visam “terceirizar”  processamentos ou até mesmo realizar os processamentos de terceiros, já que toda dapp que recebe um valor monetário suficiente deve finalizar o seu processamento e gerar um retorno para o “contratante”.

Para realização deste trabalho foram utilizadas várias referências listadas no fim deste documento juntamento com outros links julgados interessantes para aqueles interessados na tecnologia.

### Instalação de um Cliente para interação com  rede Ethereum

A instalação de um cliente com interface gráfica é bem simples, o site oficial do ethereum recomenda a utilização do mist ou da ethereum wallet, ambos disponíveis no [repositório do ethereum](https://github.com/ethereum/mist/releases), neles é possível criar uma carteira para armazenar Ether, a cripto moeda utilizada para realizar transações ou executar contratos na rede ethereum, temas abordados posteriormente.

Porém nesse tutorial como temos o intuito de utilizar uma rede privada para testar os contratos e realizar transações a melhor opção é a instalação de uma ferramenta de linha de comando (CLI), explicado abaixo.

### Instalação de uma ferramenta CLI

Existem vários clientes disponíveis para utilização das ferramentas relacionadas ao ethereum, dentre elas está o geth, uma implementação das funcionalidades na linguagem Golang, que é instalada no linux com os seguintes comandos:
```sh
sudo apt-get install software-properties-common
sudo add-apt-repository -y ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get install ethereum
```

Após a instalação do geth nós iremos criar uma rede ethereum privada.

### Criação de uma rede Ethereum privada

Como temos o intuito de testar contratos e para isso é necessária a transferência de Ether é interessante a criação de uma rede privada para manipulação do balanço geral das contas e a inclusão de transações de maneira mais rápida, com isso é necessário uma rede privada local. O que pode ser realizado através do comando:
```sh
geth --dev
```

Ou definido manualmente através de um arquivo de configuração chamado `genesis.json` que define o estado inicial da rede local privada.

Pela comodidade nós já iremos definir duas contas que estarão configuradas no estado genesis da rede privada, para é necessário obter o hash do endereço das contas e coloca-los no arquivo genesis.json. Para isso iremos criar a rede local manualmente e não através do comando geth --dev.

### Criando contas na rede Ethereum

Para criar uma conta na rede ethereum com o geth basta executar o seguinte comando:

```sh
geth account new [--datadir <diretório a sua escolha>]
```

Como iremos utilizar uma rede privaca é necessário especificar onde serão salvos os dados da conta com o parâmetro `--datadir`, o diretório escolhido deve ser diferente do diretório padrão da rede ethereum (~/.ethereum) pois lá serão guardadas as chaves encriptadas necessárias para acessar as contas com as senhas definidas durante o processo de criação das mesmas

Após a entrada do comando será requisitada uma senha que será utilzada durante as transações realizadas com a conta.

É recomendada a criação de duas contas para que seja possível realizar transações na rede privada e testar os conceitos básicos de uma rede Ethereum.

### Definição do estado inicial da rede privada

Como dito anteriormente uma rede ethereum precisa ter um estado inicial definido, isso é atingido através da criação de um arquivo genesis.json que define esse estado inicial, para isso nós iremos definir o seguinte estado:

```json
{
	"config": {
		"chainId": 10,
		"homesteadBlock": 0,
		"eip155Block": 0,
		"eip158Block": 0
	},
	"difficulty": "1",
	"gasLimit": "2100000",
	"alloc":{
	        "Endereço da conta Ethereum para a rede privada": {
			"balance": "300000"
		},
		" Endereço da conta Ethereum para a rede privada ": {
			"balance": "400000"
		}
	}
}
```
disponível em [Running a Quick Ethereum Private Network for Experimentation and Testing](https://medium.com/cybermiles/running-a-quick-ethereum-private-network-for-experimentation-and-testing-6b1c23605bce)

Basta salvar o texto acima em um arquivo genesis.json e quando iniciarmos a rede privada iremos usar desse estado inicial para trabalhar em cima de rede ethereum.

Como pode ser visto na configuração fornecida a nossa rede privada terá um identificador de número 10 e iniciará com uma dificuldade de mineração de bloco 1, também definimos as duas contas criadas anteriormente e seus balanços de ethereum.

### Inicializando a rede privada

Para inicializar a rede privada através do arquivo de configuração definido anteriormente basta utilizar os dois comandos a seguir:

```sh
geth --datadir <diretório escolhido anteroirmente> init <arquivo genesis.json> 
```

Este comando configura  a rede privada com o estado inicial da aplicação e define o datadir como o mesmo utilizada para a criação das contas anteriormente.

Agora é hora de executar o servidor que irá disponibilizar a rede privada para realização de testes, para isso utilizamos o comando:

```sh
geth --ipcpath <caminho que guardará o arquivo que representa a conexão IPC para realização das chamadas RPC> --networkid <networkId definido anteriormente> --datadir <datadir> console
```

Para testar se a rede está sendo executada corretamente e com as configurações iniciais definidas no genesis.json basta executar o mist/ethereum wallet especificando o ipcpath utilizado no último comando, por exemplo:

```sh
mist --rpc  <caminho do arquivo ipc especificado no comando anterior>
```

No próximo passo iremos criar um contrato similar ao clássico “Hello World” das outras linguagens de programação.

## Contratos

A rede ethereum é composta por dois tipos de “usuário”, as chamadas contas externas (Extrenally Owned Accounts ou EOAs) criadas anteriormente, usadas por pessoas para realizar transações monetárias (similar as transações realizadas com bitcoin) ou Contratos, que são programas executáveis guardados diretamente na rede ethereum, ativados com o envio de Gas, unidade representante de poder de processamento e adquirida com Ether.

### Criando um Contrato

Como já definimos uma rede de teste privada e já nos conectamos a ela com o mist/ethereum wallet para criar um Contrato basta utilizar o Remix IDE disponibilizado nos clientes com interface gráfica.

Contratos podem ser escritos em várias linguagens, elas geralemente são supersets de lingaugens mais comuns, entre elas estão a Solidity (JavaScript), LLL(Lisp) e Serpent(Python), o mist faz uso da Solidity para criação de contratos e neste tutorial ciraremos um contrato simples para o envio de uma mensagem ”Hello World”

[Documentação Solidity](https://solidity.readthedocs.io/en/latest/)

Para criar o contato citado acima basta usar o código em Solidity no [Remix IDE](https://remix.ethereum.org/):

```javascript
pragma solidity >=0.4.22 <0.6.0;

contract Mortal {
    /* Define variable owner of the type address */
    address owner;

    /* This constructor is executed at initialization and sets the owner of the contract */
    constructor() public { owner = msg.sender; }

    /* Function to recover the funds on the contract */
    function kill() public { if (msg.sender == owner) selfdestruct(msg.sender); }
}

contract Greeter is Mortal {
    /* Define variable greeting of the type string */
    string greeting;

    /* This runs when the contract is executed */
    constructor(string memory _greeting) public {
        greeting = _greeting;
    }

    /* Main function */
    function greet() public view returns (string memory) {
        return greeting;
    }
}
```

disponível em: [greeter contract in ethereum](https://www.ethereum.org/greeter)

E o Remix IDE irá compilar o código automáticamente e deixará o código disponível em bytecode em um arquivo para ser importado na rede Ethereum com o uso do geth.

### Publicando um Contrato na Rede Ethereum

Um contrato pode ser públicado pelo mist/ethereum wallet tanto como código fonte similar ao de cima ou como bytecode, após sua publicação ele receberá um endereço hash que pode receber Gas e Ether para que os seus processamentos sejam realizados.


Links Interessantes:
- [Usando O Mist/Ethereum Wallet para se conectar a rede Ethereum](https://medium.com/@attores/step-by-step-guide-getting-started-with-ethereum-mist-wallet-772a3cc99af4)
- [Ethereum CLI](https://www.ethereum.org/cli)
- [White Paper](https://github.com/ethereum/wiki/wiki/White-Paper)
- [Yellow Paper](https://github.com/ethereum/yellowpaper)
- [Justificativas de Design](https://github.com/ethereum/wiki/wiki/Design-Rationale)
